#!/usr/bin/env bash

echo "CertBot running..."

WORKDIR="/app/run/certBotConfigs"
FULL_CONFIG_FILE="/app/run/certBotFullConfig.conf"

LIVE_CERT_DIR="/etc/letsencrypt/live/"
NGINX_CERT_DIR="/etc/nginx/certs/"

# clean workdir
rm -rf $WORKDIR
mkdir -p $WORKDIR
cd $WORKDIR

cat $FULL_CONFIG_FILE  | sed "s/### CONFIG-END ###//g" |  awk -v RS="### CONFIG-START ###" '{ print $0 > "job" NR }'

# remove blanks
for i in $WORKDIR/*
do
   mv $i $i.ini
   i=$i.ini
   cp $i $i.back
   cat $i.back |  sed '/^$/d' | sed  's/^[ \t]*//' > $i
   rm $i.back
done

# remove empty files
find $WORKDIR/ -size  0 -print0  | xargs -0 rm



# exec certbot with each config file
for i in $WORKDIR/*
do
  certBotOut=$(certbot certonly --config $i -n --agree-tos  )
  if [ $? -ne 0 ]; then
    echo "certbot error"
  fi

  echo $certBotOut | grep "Certificate not yet due for renewal; no action taken." >> /dev/null
  if [ $? -ne 0 ]; then
    echo "renew nginx cert now"
  else
    echo "done"
  fi
done

# copy   certs
for i in $LIVE_CERT_DIR/*
do
  hostName=$(basename $i)

  if [ -d "$NGINX_CERT_DIR/$hostName" ]; then
        echo "Check certificate for $hostName"
        dirty=0

        for file in  $i/*.pem
        do
                md5Live=$(cat $file | md5sum | cut -d' ' -f 1)
                md5Nginx=$(cat $NGINX_CERT_DIR/$hostName/$(basename $file) | md5sum | cut -d' ' -f 1)

                if [ "$md5Live" != "$md5Nginx" ]
                then
                        dirty=$(($dirty+1))
                fi
        done

        if [ $dirty -eq 0 ]; then
                echo "OK"
        else
                echo "Update nginx certificate"
                rm -r $NGINX_CERT_DIR/$hostName
                cp -Lr $LIVE_CERT_DIR/$hostName $NGINX_CERT_DIR/$hostName
        fi
  else
        echo "Copy certificate for $hostName"
        cp -Lr $LIVE_CERT_DIR/$hostName $NGINX_CERT_DIR/$hostName
  fi
done



# check symlinks
cd $NGINX_CERT_DIR/
for i in $NGINX_CERT_DIR/*
do
  	if [ -d "$i" ]; then

		certFile="$i/fullchain.pem"
		keyFile="$i/privkey.pem"

		certLink="$(basename $i).crt"
		keyLink="$(basename $i).key"

		echo $certLink $keyLink

	        if [ -f "$certFile" ] && [ -f "$keyFile" ]; then
			echo "Certificate file found"

			if [ -e $certLink ] && [ -L $certLink ]; then
				echo "crt: symlink ok"
			else
				echo "crt: create symlink"
				ln -s $certFile $certLink
			fi

			if [ -e $keyLink ] && [ -L $keyLink ]; then
			    echo "key: symlink ok"
			else
			    echo "key: create symlink"
                ln -s $keyFile $keyLink
            fi
		fi
	fi
done

/app/reloadNginx.sh