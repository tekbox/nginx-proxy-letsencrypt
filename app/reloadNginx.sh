#!/bin/bash

docker exec -i  $NGINX_PROXY_CONTAINER sh -c "/usr/local/bin/docker-gen -only-exposed /app/nginx.tmpl /etc/nginx/conf.d/default.conf; /usr/sbin/nginx -s reload"