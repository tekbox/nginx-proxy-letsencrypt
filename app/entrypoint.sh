#!/bin/sh

if [ -z "${NGINX_PROXY_CONTAINER}" ]; then
    echo "NGINX_PROXY_CONTAINER is unset or set to the empty string"
    exit 1
fi

if [ -z "${LETSENCRYPT_EMAIL_DEFAULT}" ]; then
    echo "LETSENCRYPT_EMAIL_DEFAULT is unset or set to the empty string"
    exit 1
fi


exec "$@"