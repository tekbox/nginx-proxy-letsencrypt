#!/bin/bash

default_location_file="/etc/nginx/vhost.d/default"

# check well-known location
if [ ! -f $default_location_file ]; then
    touch $default_location_file
fi

# check well-known location exists
if grep -q "# nginx-proxy-letsencrypt" "$default_location_file"; then
   echo "found"
else
   echo " " >> $default_location_file
   cat /app/well_known_location >> $default_location_file
fi

# exec docker-gen 
docker-gen -watch -notify "/app/runCertBot.sh" -notify-output /app/letsencrypt.tmpl /app/run/certBotFullConfig.conf

